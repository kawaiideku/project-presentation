---
documentclass:   report
numbersections:  yes
geometry:
- top=25mm
- left=30mm
- right=30mm
header-includes: |
    \usepackage{fancyhdr}
    \usepackage{graphicx}
    \usepackage[utf8]{inputenc}

    \graphicspath{ {./ppt-slides/} }

    \pagestyle{fancy}
    \fancyhf{}
    \fancyhead[R]{\textit{Real-Time Video Super Resolution}}
    \fancyfoot[L]{
    \textit{Dept. of CSE, RIT Kottayam}
    }
    \fancyfoot[R]{
	    \thepage
    }
    \setlength{\headheight}{1pt}
    \renewcommand{\headrulewidth}{0.4pt}
    \renewcommand{\footrulewidth}{0.4pt}

indent:          12.5mm
linestretch:     1.5
papersize:       a4
mainfont:        Times New Roman
fontsize:        12pt

---

\pagenumbering{roman}
\addtocontents{toc}{\textbf{Abstract}~\hfill\textbf{iii}\par}

\setcounter{page}{4}
\tableofcontents

\cleardoublepage
\addcontentsline{toc}{chapter}{\listfigurename}
\listoffigures
\listoftables

\newpage
\pagenumbering{arabic}


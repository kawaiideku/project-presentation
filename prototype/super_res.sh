#!/usr/bin/env bash

SRC_DIR=$1
DEST_DIR=$2
mkdir -p "${DEST_DIR}/sr_${SRC_DIR}"
for i in ${SRC_DIR}/*
do
	#add a mkdir for making sr_${SRC_DIR} in DEST_DIR
	waifu2x-ncnn-vulkan -i $i -o "${DEST_DIR}/sr_${i}" -n 2
done

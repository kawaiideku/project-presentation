#!/usr/bin/env bash

SRC_DIR=$1
DEST_FILE=$2
PREFIX="240p_mha_480p_trimmed.mp4"
ffmpeg -loop 1 -i ${SRC_DIR}/"${PREFIX}%d.bmp" -c:v libx264 -pix_fmt yuv420p $DEST_FILE

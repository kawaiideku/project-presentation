all:
	pandoc format.md main.md footer.md -o pdfs/report_main.pdf
full:
	pdfunite pdfs/report_cover_page.pdf pdfs/report_front_page.pdf \
	pdfs/report_certificate.pdf pdfs/report_main.pdf \
	pdfs/single_file_report_and_ppt.pdf

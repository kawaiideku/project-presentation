# INTRODUCTION

Image and video super-resolution (SR) are long-standing challenges of signal
Image and video super-resolution (SR) are long-standing challenges of signal
processing. SR aims at recovering a high-resolution (HR) image or video from
its low-resolution (LR) version, and finds direct applications ranging from
medical imaging to satellite imaging , as well as facilitating tasks such as
face recognition . The reconstruction of HR data from a LR input is however a
highly ill-posed problem that requires additional constraints to be solved.
While those constraints are often application-dependent, they usually rely on
data redundancy. HD Video Streaming through the internet requires transmission
of large amounts of encoded video/voice data, which could lead to considerable
buffering and stuttering unless the user has a really fast internet connection.
Freddie proposes a potential solution to the above mentioned scenarios.

## PROBLEM STATEMENT

To solve the problem of video streaming consuming large amounts of data for the
average user and the lack of high quality content for 4k and 8k screens.
Streaming HD video consumes large amounts of data and is not possible if the
user has bad network coverage or has a limited data cap. Freddie solves this
problem by transmitting low resolution video over the internet to the end user
and then upscaling it using the user's computational resources in real time.

# LITERATURE SURVEY

Video SR methods have mainly emerged as adaptations of image SR techniques.
Kernel regression methods have been shown to be applicable to videos using 3D
kernels instead of 2D ones . Dictionary learning approaches, which define LR
images as a sparse linear combination of dictionary atoms coupled to a HR
dictionary, have also been adapted from images to videos . Another approach is
example-based patch recurrence, which assumes patches in a single image or
video obey multi-scale relationships, and therefore missing high-frequency
content at a given scale can be inferred from coarser scale patches.This was
successfully presented by Glasner et al. for image SR and has later been
extended to videos .

When adapting a method from images to videos it is usually beneficial to
incorporate the prior knowledge that frames of the same scenes of a video can
be approximated by a single image and a motion pattern. Estimating and
compensating motion is a powerful mechanism to further constrain the problem
and expose temporal correlations. It is therefore very common to find video SR
methods that explicitly model motion through frames. A natural choice has been
to preprocess input frames by compensating interframe motion using displacement
fields obtained from off the-shelf optical flow algorithms . This nevertheless
requires frame preprocessing and is usually expensive.

Alternatively, motion compensation can also be performed jointly with the SR
task, as done in the Bayesian approach of Liu et al. by iteratively estimating
motion as part of its wider modeling of the downscaling process. The advent of
neural network techniques that can be trained from data to approximate complex
nonlinear functions has set new performance standards in many applications
including SR. Dong et al. proposed to use a convolutional neural network (CNN)
architecture for single image SR that was later extended by Kappeler et al. in
a video SR network (VSRnet) which jointly processes multiple input frames.
Additionally, compensating the motion of input images with a total variation
(TV)-based optical flow algorithm showed an improved accuracy. Joint motion
compensation for SR with neural networks has also been studied through
recurrent bidirectional networks .

## REAL-TIME SINGLE IMAGE AND VIDEO SUPER-RESOLUTION USING AN EFFICIENT SUB-PIXEL CONVOLUTIONAL NEURAL NETWORK

> [Wenzhe Shi, Jose Caballero, Ferenc Husz´ar, Johannes Totz, Andrew P. Aitken, Rob Bishop, Daniel Rueckert, Zehan Wang]

This paper argues that traditional methods for SR, in which the
super-resolution operation is performed in the HR space using a single filter,
are sub-optimal and add computational complexity. In their stead, it introduces
an efficient sub-pixel convolution layer which learns an array of upscaling
filters to upscale the final LR feature maps into the HR output. The proposed
efficient sub-pixel convolutional neural network (ESPCN) has two convolution
layers for feature maps extraction and a sub-pixel convolution layer that
aggregates the feature maps from LR space and builds the SR image in a single
step.

![CNN architecture](images/cnn.png)

In this 3-layer architecture, upscaling is performed by the last layer. It
first applies an l-layer convolutional neural network directly to the
low-resolution image and then applies a sub-pixel convolution layer that
upscales the low-resolution feature maps to produce the high-resolution image.
They exploit local spatial correlations to perform image SR and additional
temporal correlations to perform video SR.

## REAL-TIME VIDEO SUPER-RESOLUTION WITH SPATIO-TEMPORAL NETWORKS AND MOTION COMPENSATION

> [Jose Caballero, Christian Ledig, Andrew Aitken, Alejandro Acosta, Johannes Totz, Zehan Wang, Wenzhe Shi]

Convolutional neural networks have enabled accurate image super-resolution in
real-time. This paper introduces the use of spatio-temporal subpixel
convolution networks that effectively exploit temporal redundancies and improve
reconstruction accuracy while maintaining real-time speed. It assumes input
data to be a block of spatio-temporal information, such that instead of a
single input frame, a sequence of consecutive frames is considered.

The paper compares different spatio-temporal models like early fusion and slow
fusion, for the joint processing of multiple consecutive video frames. It was
found that while slow fusion yields better performance and is faster, it is 3%
less accurate than early fusion models. The paper also proposes a novel joint
motion compensation and video super-resolution algorithm that is orders of
magnitude more efficient than competing methods, relying on a fast
multi-resolution spatial transformer module that is end-to-end trainable. These
contributions provide both higher accuracy and temporally more consistent
videos.

Relative to single-frame models, spatio-temporal networks can either reduce the
computational cost by 30% whilst maintaining the same quality or provide a
0.2dB gain for a similar computational cost. Results on publicly available
datasets demonstrate that the proposed algorithms surpass current
state-of-the-art performance in both accuracy and efficiency.

![Proposed design for video SR](images/proposed_SR_design.png)

The motion estimation and ESPCN modules are learnt end-to-end to obtain a
motion compensated and fast algorithm

## VIDEO SUPER-RESOLUTION WITH CONVOLUTIONAL NEURAL NETWORKS

> [Armin Kappeler, Seunghwan Yoo, Qiqin Dai, and Aggelos K. Katsaggelos]

This paper proposes a CNN that is trained on both the spatial and the temporal
dimensions of videos, to enhance their spatial resolution. The paper argues
that CNN-based algorithms are much faster than traditional approaches, since
super-resolving an image is a purely feed-forward process, once the CNN is
trained. Moreover, the proposed algorithm requires only a small video database
for training to achieve a very promising performance.

The proposed CNN uses multiple LR frames as input to reconstruct one HR output
frame. It is also shown that by using images to pre-train the model, a
relatively small video database is sufficient for the training of the model to
achieve and even improve upon the current state-of-the-art. Additionally, an
adaptive motion compensation scheme is applied to handle fast-moving objects
and motion blur in videos.

\newpage

![Video SR architectures](images/SRarchitectures.png)

- (a) the three input frames are concatenated (Concat Layer) before layer 1 is applied.
- (b) concatenates the data between layers 1 and 2, and
- (c) between layers 2 and 3.

| Paper                                                                                                          | Approach                                                                | Whether Real time or not                                  |     |
| -------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------- | --------------------------------------------------------- | --- |
| VESPCN                                                                                                         | Spatio\-Temporal Sub\-pixel CNN                                         | Real time                                                 |     |
| Video Super\-Resolution with Convolutional Neural Networks                                                     | Preprocessing of LR images with bicubic upscaling                       | Not real time                                             |     |
| Real\-Time Single Image and Video Super\-Resolution Using an Efficient Sub\-Pixel Convolutional Neural Network | Sub\-pixel convolution layer which learns an array of upscaling filters | Yes, but does not take advantage of temporal correlations |     |
|                                                                                                                |                                                                         |                                                           |     |

Table: Literature survey summary

# PROPOSED ARCHITECTURE

![Architecture Diagram](images/architecture_diagram.png)

This is a high level architecture of the software. A video file is taken as the
input and a video stream is produced as output. The software as a whole can be
viewed as 3 independent components each of which have their own complexity.

## PATCH EXTRACTOR AND MERGER

The patch extractor/merger does the task of exploiting temporal redundancy in a
video. The mathematical difference of pixel values between subsequent frames is
taken and only those pixels are upscaled, which increases the speed of super
resolution. The input video file is read as a stream of frames, taking care not
to cause a memory overflow. The keyframes are also identified so that time
won't be wasted on calculating too big of a difference between frames. Future
implementations can make use of motion vectors present in the compressed video
itself so that the performance can be improved without even calculating the
difference between frames.

## PATCH UPSCALER

This is where the magic (super resolution) happens. A CNN is used to process
the incoming frames and upscale them using bicubic interpolation.

However, this method poses a serious problem. In the case of pixels that have
the same values in successive frames, their difference would be found to be
zero upon subtracting the corresponding numpy arrays. The Neural net needs to
be taught to ignore these zero values so as to avoid upscaling the same pixel
again, thereby greatly increasing efficiency.

The CNN is trained with intermediate frames so that it gets better at upscaling
them.

|                                            | Time taken |
| ------------------------------------------ | ---------- |
| Freddie \(exploits temporal redundancies\) | 3:37 min   |
| Frame by frame upscaling                   | 18:34 min  |

Table: Performance comparison

Almost 600% efficiency compared to frame by frame upscaling of content.

# IMPLEMENTATION DETAILS

Most of Freddie's codebase is implemented in the Python programming language.

## PATCH EXTRACTOR AND MERGER

Libraries used:

- PyAV : For preprocessing video
- Numpy : For representing frames of video in a numerical structure

The video file is opened as a Python generator object and therefore does not
fill the memory with the entirety of the video. Frames are extracted when
needed as a stream. Frame difference is obtained using numpy utility functions
and upscaled frames are merged together with the same.

![Temporal Redundancy Demo](images/temporal_redundancy.png)

## PATCH UPSCALER

Libraries used:

- Tensorflow : For building the CNN in Python
- ncnn-vulkan : Neural net implementation in c++

### FRAME UPSCALING ALGORITHM: WAIFU2X

Waifu2x is an image super resolution implementation using deep convolutional
networks. For any given low resolution pixel, there might be a multiplicity of
solutions. Waifu2x tries to constrain the solution space by employing an
example-based strategy. For this, it makes use of dictionaries that are
directly presented as low and high resolution exemplar pairs, and the nearest
neighbour of the input patch is found in the low-resolution space, with its
corresponding high resolution patch used for reconstruction.

This method makes use of minimal preprocessing- only upscaling the
low-resolution image to the desired size using bicubic interpolation. The
patches are the focus of optimization- patch extraction and patch aggregation
steps are considered as pre- and post-processing steps and handled separately.
The restoration problems are more or less denoising-driven.

Conceptually, the CNN is formed by three main operations:

- Patch extraction and representation: Patches are extracted from the low
  resolution image and each patch is represented as a high-dimensional vector,
  say of dimension n1. These vectors comprise a set of feature maps, with the
  number of feature maps being equal to the dimensionality of the vector(n1).
  This is equivalent to convolving the image by a set of filters, each of which
  is a basis.

- Non-linear mapping: This operation nonlinearly maps each
  high-dimensional(n1) vector onto another high-dimensional(n2) vector(which is
  often equivalent to applying n2 filters, which have a trivial spatial support
  of 1x1). Each mapped vector is conceptually the representation of a
  high-resolution patch. These vectors comprise another set of feature maps. More
  convolutional layers could be added here to increase the non-linearity, but
  that would increase the complexity of the model and thus demand more training
  time.

- Reconstruction: This operation aggregates the above high-resolution
  patch-wise representations to generate the final high-resolution image. The
  predicted overlapping high-resolution patches are often averaged to produce the
  final full image. This image is expected to be similar to the ground truth
  high-resolution image.

This structure has been designed with simplicity in mind, and yet provides
superior accuracy compared with state-of-the-art example-based methods. With a
moderate number of filters and layers, it is highly fast since it is fully
feed-forward and does not need to solve any optimization problem on usage.
Furthermore, the proposed network can cope with three channels of color images
simultaneously to achieve improved super-resolution performance. Experiments
show that the restoration quality of the network can be further improved when

- Larger and more diverse datasets are available, and/or
- A larger and deeper model is used.

![SRCNN](images/srcnn.png)

Given a low-resolution image, the first convolutional layer of the SRCNN
extracts a set of feature maps. The second layer maps these feature maps
nonlinearly to high-resolution patch representations. The last layer combines
the predictions within a spatial neighbourhood to produce the final
high-resolution image.

#### FRAME BY FRAME UPSCALING

The comparison of time difference between the chainer implementation and ncnn
implementation is showcased in the table below. Chainer is an open source deep
learning framework written purely in Python on top of Numpy and CuPy Python
libraries. Ncnn is a high-performance neural network inference computing
framework optimized for mobile platforms.

| Time to upscale a single frame | On CPU  | On GPU |
| ------------------------------ | ------- | ------ |
| Waifu2x-chainer                | 192.22s | 27.33s |
| Waifu2x-ncnn                   | 3.11s   | 1.104s |

Table: waifu2x implementation comparison

The above benchmarks were performed on a system with CPU: Intel Core i5 8300H and
GPU: Nvidia GTX 1050. The above mentioned results are not nearly fast enough to
actually perform real time video super resolution as a normal video footage is
usually shot at 24 fps, 30 fps or even 60 fps. The algorithm should be
optimized greatly before a real time video super resolution implementation can
be realised.

![Frame by frame upscaling](images/frame_by_frame_up.png)

## GRAPHICAL USER INTERFACE

This project intends to make a complete graphical user interface to make this
project accessible to all kinds of people.

Library used:

PyQt - Uses the popular Qt framework for building fast and responsive gui in python itself.

![GUI Running](images/gui_main.png)

## PLAYBACK CONTROLLER

In order to show frames upscaled in real time, the frames need to be buffered intelligently. It's a sort of producer-consumer problem
that is solved here with a bounded buffer and buffer management algorithm.
For simplicity and to reduce the size of the packaged software, Freddie plays the upscaled video in the default media player of the system.

![Playback controller](images/playback_controller.png)

# EVALUATION MEASURES

## PSNR ([0-1] Higher is better)

Peak signal-to-noise ratio, often abbreviated PSNR, is an engineering term for
the ratio between the maximum possible power of a signal and the power of
corrupting noise that affects the fidelity of its representation. Because many
signals have a very wide dynamic range, PSNR is usually expressed in terms of
the logarithmic decibel scale.

![PSNR formula](images/psnr.png)

## SSIM (Higher is better)

SSIM is used for measuring the similarity between two images. The SSIM index is
a full reference metric; in other words, the measurement or prediction of image
quality is based on an initial uncompressed or distortion-free image as
reference. SSIM is designed to improve on traditional methods such as peak
signal-to-noise ratio (PSNR) and mean squared error (MSE).

The difference with respect to other techniques mentioned previously such as
MSE or PSNR is that these approaches estimate absolute errors; on the other
hand, SSIM is a perception-based model that considers image degradation as
perceived change in structural information, while also incorporating important
perceptual phenomena, including both luminance masking and contrast masking
terms. Structural information is the idea that the pixels have strong
inter-dependencies especially when they are spatially close. These dependencies
carry important information about the structure of the objects in the visual
scene. Luminance masking is a phenomenon whereby image distortions (in this
context) tend to be less visible in bright regions, while contrast masking is a
phenomenon whereby distortions become less visible where there is significant
activity or "texture" in the image.

![SSIM formula](images/ssim.png)

## EXPERIMENTAL SETUP

SSIM and PSNR values were calculated using the `ffmpeg` tool.

### To calculate SSIM

    ffmpeg -i upscaled_video.mp4 -i original_video.mp4 -filter_complex ssim output.mp4

### To calculate PSNR

    ffmpeg -i upscaled_video.mp4 -i original_video.mp4 -filter_complex psnr output.mp4

### Data

Currently, no reference dataset is used. A single sample video is only used to
measure the improvement, which is why high scores are obtained compared to
other state-of-the-art implementations.

![Freddie GUI and CPU usage on idle](images/gui.png)

# COMPARISON WITH STATE OF THE ART

The following table portrays the comparison of 2 models which aim to perform
video super resolution (not real-time). These models aim to upscale real world
footage, while freddie upscales anime content. This would be similar to an
apples to orange comparison but has still been included as there are no other
implementations of video super resolution to compare with.

|      |          |        |        |
| ---- | -------- | ------ | ------ |
| RANK | METHOD   | PSNR   | SSIM   |
| 1    | EDVR     | 27\.35 | \.8264 |
| 2    | VSR\-DUF | 27\.31 | \.832  |

Table: Comparison with state-of-the-art

The PSNR and SSIM of super resolution performed using Freddie is as follows:
- PSNR: 37.904
- SSIM: 0.9729

As you can see, the output of Freddie yields very impressive values. One of the
reasons which Freddie achieves such high results is because Freddie specializes
in upscaling of anime content, whose dynamic range is far lower than real world
footage. It is still very impressive nonetheless especially considering the
fact that the time taken by Freddie to produce such high quality output is but
a mere fraction of the time that is required by these other models.

## PRELIMINARY EXPERIMENTS

The preliminary experiment consisted of extracting each frame from a sample
video, feeding each frame into the super resolution model, storing each of
these super resolved frames and then converting these frames back into a video
using ffmpeg.

Waifu2x model, which specializes in 2D animated image upscaling was used as the
base model. The ncnn implementation of waifu2x based on vulkan
graphics API was used for testing, as it was found to be the most efficient
implementation of waifu2x.

    ffmpeg -i ${SRC_FILE} $FRAME_DIR/"${name}%d".bmp

The frames were stored as bitmap so that no information is lost.

To upscale all the extracted frames, the following script was used:

```
SRC_DIR=$1
DEST_DIR=$2
for i in ${SRC_DIR}/*
do
    name=$(echo "$i" | cut -f 1 -d '.')
    #add a mkdir for making sr_${SRC_DIR} in DEST_DIR
    waifu2x-ncnn-vulkan -i $i -o "sr_${name}.png" -n 2 -g 0
done

```

The upscaled frames are then joined together to form a video using the script:

```
SRC_DIR=$1
DEST_FILE=$2
PREFIX=$3
ffmpeg -framerate 23.976 -i "${SRC_DIR}/${PREFIX}%d.png" $2
```

![Calculating frame difference](images/opencv_frame_diff.png)

## OUTPUT SCREENSHOTS

![Screenshot 1](images/outputscrot1.png)
![Screenshot 2](images/outputscrot2.png)
![Screenshot 3](images/outputscrot3.png)
![Screenshot 4](images/outputscrot4.png)

# IMPLEMENTATION PLAN

![Gantt Chart](images/gantt_chart.png)

They checked out the previous implementations of this vast field during the
months of September and October. They finally decided on VESPCN for Freddie's
implementation.

They plan to make a repository of different models trained with videos of
different artstyles for optimal quality. Though, further testing is needed in
this regard. The final result will be a web player that consumers can readily
use.

# CONCLUSION

The spatio-temporal models used are shown to facilitate an improvement in
reconstruction accuracy and temporal consistency or reduce computational
complexity relative to independent single frame processing. The models
investigated are extended with a motion compensation mechanism based on spatial
transformer networks that are efficient and jointly trainable for video SR.
Results obtained with approaches that incorporate explicit motion compensation
are demonstrated to be superior in terms of PSNR and temporal consistency
compared to spatio-temporal models alone, and outperform the current state of
the art in video SR.

When deployed on the cloud, users will be able to stream HD videos over the
internet without buffering while simply transferring the equivalent data of a
240p video, while still being able to enjoy it in HD resolution. Freddie
specializes in the upscaling of 2D animation videos and yields very impressive
results with almost 90% reconstruction accuracy when upscaled to twice the
original resolution of the video. Thus, Freddie has lived up to its name by
finally providing a rad enhancer for dynamic digital immersive entertainment
and media consumption.

\clearpage

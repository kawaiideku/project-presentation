\chapter*{REFERENCES}
\addcontentsline{toc}{chapter}{REFERENCES}

> [1] A. Ahmadi and I. Patras. Unsupervised convolutional neural
> networks for motion estimation. IEEE International Conference on Image Processing (ICIP), pages 1629-1633, 2016.

> [2] T. Brox, N. Papenberg, and J. Theyickert. High accuracy optical flow
> estimation based on a theory for warping. European
> Conference on Computer Vision (ECCV), 4:25-36, 2004.

> [3] J. Bruna, P. Sprechmann, and Y. LeCun. Super-resolution
> with deep convolutional sufficient statistics. In International
> Conference On Learning Representations (ICLR), 2016.

> [4] Q. Dai, S. Yoo, A. Kappeler, and A. K. Katsaggelos.
> Dictionary-based multiple frame video super-resolution.
> IEEE International Conference on Image Processing (ICIP),
> pages 83-87, 2015.

> [5] H. Demirel and G. Anbarjafari. Discrete wavelet transform-
> based satellite image resolution enhancement. IEEE Trans-
> actions on Geoscience and Remote Sensing, 49(6):1997-
> 2004, 2011.

> [6] C. Dong, C. C. Loy, K. He, and X. Tang. Image super-resolution using deep
> convolutional networks. IEEE Transactions on Pattern Analysis and Machine
> Intelligence (TPAMI), 38(2):295-307, 2015.

> [7] C. Dong, C. C. Loy, and X. Tang. Accelerating the super-resolution
> convolutional neural network. In European Conference on Computer Vision (ECCV),
> pages 391-407. Springer International Publishing, 2016.

> [8] A. Dosovitskiy and T. Brox. Generating images with perceptual similarity
> metrics based on deep networks.

\chapter*{\large APPENDIX A \break \large PRESENTATION SLIDES}
\pagenumbering{roman}
\addcontentsline{toc}{chapter}{APPENDIX A PRESENTATION SLIDES}

![](ppt-slides/slide-1.png)

![](ppt-slides/slide-2.png)

![](ppt-slides/slide-3.png)

![](ppt-slides/slide-3.png)

![](ppt-slides/slide-4.png)

![](ppt-slides/slide-5.png)

![](ppt-slides/slide-6.png)

![](ppt-slides/slide-7.png)

![](ppt-slides/slide-8.png)

\chapter*{\large APPENDIX B \break \large CODE SNIPPETS}
\addcontentsline{toc}{chapter}{APPENDIX B CODE SNIPPETS}

    PyAVutils.py

    from matplotlib import pyplot as plt
    import numpy as np
    import av

    def display_frame(frame):
    """Display frame of type av.VideoFrame with matplotlib
    :frame: object of class av.VideoFrame
    :returns: nothing
    """
    np_frame = frame.to_ndarray(format='rgb24')
    display_np_frame(np_frame)

    def display_np_frame(np_frame):
    """Display numpy array with matplotlib
    :np_frame: numpy array
    :returns: nothing
    """
    plt.imshow(np_frame)
    plt.show()

    def np_frame_diff(frame1, frame2):
    """Get difference of the frames

        :frame1: object of class av.VideoFrame
        :frame2: object of class av.VideoFrame
        :returns: difference of 2 frames as av.VideoFrame

        """
        np_frame1 = frame1.to_ndarray(format='rgb24')
        np_frame2 = frame2.to_ndarray(format='rgb24')
        diff = np.subtract(np_frame1, np_frame2)
        return diff

    def start_of_scene(frame):
    """Check if a frame is the start of scene with keyframe heuristic

        :frame: Object of class av.VideoFrame
        :returns: True if frame is keyframe

        """
        return frame.key_frame

    def make_generator(filename):
    """make a generator to retreive frames from video

        :filename: path to video file as string
        :returns: generator object that generates av.VideoFrame objects

        """
        container = av.open('v/mha_s4_ep2.mp4')
        return container.decode(video = 0)
